<tr id="row-<?php echo $value['id']; ?>">
    <td>
        <div>
            <span><a href="#modalEdit" class="editRow showModal" id="<?php echo $value['id']; ?>">Edit</a></span>
            <span class="imgRowTitle"><?php echo $value['title']; ?><span>
        </div>
    </td>
    <td>
        <div>
            <img src="<?php echo $value['img']; ?>" alt="<?php echo $value['title']; ?>">
        </div>
    </td>
    <td>
        <div><span class="imgRowFileName"><?php echo $value['file_name']; ?><span></div>
    </td>
    <td>
        <div>
            <?php echo $value['date']; ?>
        </div>
    </td>
    <td>
        <div>
            <input type="checkbox" value="<?php echo $value['id']; ?>" class="dataCheckbox">
        </div>
    </td>
</tr>