<div class="modal-container" id="modalUpload">
    <div class="modal">
        <span class="modal-close">
            <em class="fa fa-times"></em>
        </span>
        <div class="modal-content">
            <form method="post" enctype="multipart/form-data">
                <h3>Upload your image</h3>

                <label for="imageText">Image Title</label>
                <input type="text" id="imageText" placeholder="Image Title">

                <div class="upload-container">
                    <label for="imageFile" class="imageFileLabel">
                        Choose an image
                    </label>
                    <input type="file" id="imageFile" name="image-upload" accept="image/*">
                </div>
                <br>
                <button class="btn" id="uploadBtn">
                    Upload
                </button>
            </form>

            <div class="img-loader">
                <img src="/assets/images/loader.svg">
            </div>
        </div>
    </div>
</div>