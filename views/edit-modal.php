<div class="modal-container" id="modalEdit">
    <div class="modal">
        <span class="modal-close">
            <em class="fa fa-times"></em>
        </span>
        <div class="modal-content">
            <form method="post">
                <h3>Edit image details</h3>

                <label for="imageTitle">Image Title</label>
                <input type="text" id="imageTitle" placeholder="Image Title">

                <br>
                <label for="imageFilename">Image Filename</label>
                <input type="text" id="imageFilename" placeholder="Image Filename">

                <br>
                <input type="hidden" id="imageID">
                <button class="btn" id="editBtn" disabled>
                    Update
                </button>
            </form>

            <div class="img-loader">
                <img src="/assets/images/loader.svg">
            </div>
        </div>
    </div>
</div>