<div class="image-upload-table">
    <table>
        <thead>
            <tr>
                <th>
                    <div>
                        Title
                    </div>
                </th>
                <th>
                    <div>
                        Thumbnail
                    </div>
                </th>
                <th>
                    <div>
                        Filename
                    </div>
                </th>
                <th>
                    <div>
                        Date added
                    </div>
                </th>
                <th>
                    <input type="checkbox" id="selectAll">
                </th>
            </tr>
        </thead>
        <tbody>
            <?php echo $value; ?>
        </tbody>
    </table>
</div>