<?php use TestIU\Extras; ?>

<!doctype html>
<html lang="en-US" >
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Test - Image File Uploading App</title>
    <?php Extras::assets_on_header(); ?>
</head>

<body>
    <div class="container">
        <header class="site-header">
            <h1 class="site-title">
                Test
            </h1>
            <p class="site-description">
                Image File Uploading App
            </p>
        </header>