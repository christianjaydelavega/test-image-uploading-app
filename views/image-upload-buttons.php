<br>
<div class="image-upload">
    <button class="btn showModal" href="#modalUpload">
        <i class="fa fa-arrow-circle-up"></i>
        Upload an Image
    </button>

    <button class="btn btn-danger" id="deleteEntries">
        <i class="fa fa-trash-alt"></i>
        Delete
    </button>
</div>