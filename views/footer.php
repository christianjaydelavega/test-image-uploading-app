<?php use TestIU\Extras; ?>
   
            <footer>
                <div class="site-info">
                    <div class="site-name">
                        <span class="highlighted">Test</span> 
                        - Image File Uploading App
                    </div>
                    <div class="site-author">
                        Created by 
                        <span class="highlighted">Christian Dela Vega</span> 
                    </div>
                </div>
            </footer>
            
            <?php Extras::get_view('add-modal'); ?>

            <?php Extras::get_view('edit-modal'); ?>

            <?php Extras::assets_on_footer(); ?>
        </div>
    </body>
</html>