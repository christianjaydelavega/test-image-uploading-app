import jQuery from 'jquery';
import ImageUpload from './image-upload';

(function($){
    'use strict';

    $(document).ready( new ImageUpload().init() );
}(jQuery));