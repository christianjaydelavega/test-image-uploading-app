import $ from 'jquery';

class ImageAjax {
    constructor()
    {
        this.ajaxURL = '/inc/classes/class-ajax-actions.php';
    }

    upload( eL, ajaxAction )
    {   
        let formData = new FormData();
        formData.append('image_title', $( eL ).parent().find('input[type="text"]').val());
        formData.append('image', $( eL ).parent().find('input[type="file"]').prop("files")[0]);
        formData.append('action', ajaxAction);

        return this.ajaxFnc({
            form: formData
        });
    }

    delete( eL, img, ajaxAction )
    {
        let formData = new FormData();
        formData.append('ids', eL);
        formData.append('images', img);
        formData.append('action', ajaxAction);

        return this.ajaxFnc({
            form: formData
        });
    }

    update( eL, ajaxAction )
    {
        let formData = new FormData();
        formData.append('id', eL.id);
        formData.append('title', eL.title);
        formData.append('file', eL.file);
        formData.append('old_file', eL.oldFile);
        formData.append('action', ajaxAction);

        return this.ajaxFnc({
            form: formData
        });
    }

    validateName( data )
    {
        let formData = new FormData();
        formData.append('name', data);
        formData.append('action', 'is_name_valid');
        
        return this.ajaxFnc({
            form: formData
        });
    }

    ajaxFnc( data )
    {
        return $.ajax({
            url: this.ajaxURL,
            type: 'POST',
            data: data.form,
            action: data.ajax_action,
            contentType: false,
            cache: false,
            processData:false,
            dataType: 'json'
        });
    }
}

export default ImageAjax;