import $ from 'jquery'; 
import Modal from './modal';
import Validation from './validation';
import ImageAjax from './image-ajax';

class ImageUpload {

    constructor()
    {
        this.eventsArr = {
            '.showModal': {
                'click': 'showModal'
            },
            '.modal-close': {
                'click': 'closeModalBtn'
            },
            '.modal-container': {
                'click': 'closeModalCntr'
            },
            '#imageFile': {
                'change': 'updateLabel'
            },
            '#uploadBtn': {
                'click': 'validateImage'
            },
            '#selectAll': {
                'click': 'toggleCheckbox'
            },
            '#deleteEntries': {
                'click': 'deleteData'
            },
            '.editRow': {
                'click': 'setEditData'
            },
            '#imageTitle, #imageFilename': {
                'input': 'frmChange'
            },
            '#editBtn': {
                'click': 'validateEdit'
            }
        };
        
        this.oldForm = {};
        this.fileExt = '';
        this.modal = new Modal();
        this.validation = new Validation();
        this.imageAjax = new ImageAjax();
    }
    
    init()
    {
        $.each( this.eventsArr, (evt, fnc) => {
           $.each( fnc, (key, val) => {
                this[val] = this[val].bind(this);
               $( 'body' ).on(key, evt, this[val]);
           })
        });
    }

    showModal( event )
    {
        this.removeNotif( $( event.target ).attr('href') );
        this.modal.show( $( event.target ).attr('href') );
    }

    closeModalBtn( event )
    {
        this.modal.close( $( event.target ).parents(':eq(1)') );
        this.animateNewRows();
    }

    closeModalCntr( event )
    {
        if(event.target != event.currentTarget){
            return;
        }

        this.modal.close( $( event.target ).find('.modal') );
        this.animateNewRows();
    }

    updateLabel( event )
    {
        let target = event.target,
        fileName = $( target ).val().split("\\").pop();

        $( target ).siblings(".imageFileLabel").html(fileName);
    }

    validateImage( event )
    {
        event.preventDefault();
        $( event.target ).prop('disabled', true);
        this.showLoader( event );
        this.removeNotif( $( event.target ).parent() );

        let file = $( '#imageFile' ).val().split('.').pop().toLowerCase();
        
        if(!this.validation.required(file)){
            this.validation.errorNotif( event, 'requiredFile');
            $( event.target ).prop('disabled', false);
            this.hideLoader( event );

            return false;
        }

        if(!this.validation.fileType(file)){
            this.validation.errorNotif( event, 'type');
            $( event.target ).prop('disabled', false);
            this.hideLoader( event );

            return false;
        }
        
        this.imageUpload( event );
    }
    
    imageUpload( event )
    {
        this.imageAjax.upload( event.target, 'upload_image')
        .done((response) => {
            let imgName = response.data.title;

            $( event.target ).after('<div class="alert">'+ imgName +' uploaded successfully!</div>');

            this.addRow( response.data );
            this.modal.reset( event.target );
            $( event.target ).prop('disabled', false);
            this.hideLoader( event );
        })
        .fail(() => {
            this.validation.errorNotif( event );
        });
    }

    toggleCheckbox( event )
    {
        $('input[type="checkbox"]').prop('checked', $(event.target).is(':checked'));
    }

    deleteData( event )
    {
        let checkboxArr = [],
        imgArr = [],
        emptyCont = '<h2>No uploads yet.</h2><span class="highlighted">Upload one now <i class="fa fa-hand-point-up"></i><span></span></span>';

        $('.dataCheckbox:checkbox:checked').each(function(){
            let checkID = $(this).val(),
            img = $(this).parents(':eq(2)').find('img').attr('src');

            checkboxArr.push(checkID);
            imgArr.push(img);
        });

        if(checkboxArr === undefined || checkboxArr.length == 0){
            return;
        }

        $('#selectAll').prop('checked', false);

        this.imageAjax.delete( checkboxArr.toString(), imgArr.toString(), 'delete_image')
        .done(() => {
            $.each(checkboxArr, function(i, val){
                setTimeout(function(){
                    $('#row-'+val+' td').animate({ paddingTop: 0, paddingBottom: 0 }, 500)
                    .children()
                    .slideUp(500, function(){
                        $(this).closest('tr').remove().promise().done(() => {
                            let rowsCnt = $('.image-upload-table tr').length;

                            if(rowsCnt <= 1){
                                $('.image-upload-table table th')
                                .children()
                                .slideUp(500, function() {
                                    $(this).closest('table')
                                    .replaceWith(emptyCont);
                                });
                            }
                        });
                    });
                }, 500 * i);
            });
        })
        .fail(() => {
            this.validation.errorNotif( event );
        });
    }

    setEditData( event )
    {
        let target = $( event.target ),
        parent = target.parents(':eq(3)'),
        oldTitle = parent.find('.imgRowTitle').text().trim(),
        oldFileName = parent.find('.imgRowFileName').text(),
        entryID = $( event.target ).attr('id');
        this.fileExt = oldFileName.split('.').pop().toLowerCase();
        this.oldForm = {
            'title': oldTitle,
            'file': oldFileName.split('.').slice(0, -1).join('.').trim()
        };

        $( '#imageTitle' ).val(oldTitle);
        $( '#imageFilename' ).val(oldFileName.split('.').slice(0, -1).join('.').trim());
        $( '#imageID' ).val(entryID);
    }

    frmChange( event )
    {
        let frmData = {
            'title': $( '#imageTitle' ).val(),
            'file': $( '#imageFilename' ).val()
        };

        if(this.validation.frmValid( this.oldForm, frmData ) === false){
            $( '#editBtn' ).prop('disabled', true);
            return;
        }

        $( '#editBtn' ).prop('disabled', false);
    }
    
    validateEdit( event )
    {
        event.preventDefault();
        $( event.target ).prop('disabled', true);
        this.showLoader( event );
        this.removeNotif( $( event.target ).parent() );

        let file = $( '#imageFilename' ).val(),
        title = $( '#imageTitle' ).val(),
        oldFileName = this.oldForm.file + '.' + this.fileExt,
        fileName = file + '.' + this.fileExt;

        if(!this.validation.required(file)){
            this.validation.errorNotif( event, 'requiredFilename');
            $( event.target ).prop('disabled', false);
            this.hideLoader( event );
            
            return;
        }

        if( this.oldForm.file == file ){
            this.editData({
                'id': $( '#imageID' ).val(),
                'title': title,
                'file': fileName,
                'oldFile': oldFileName,
                'fileName': file
            }, event );

            return;
        }

        this.imageAjax.validateName( fileName )
        .done((response) => {
            if(response.status == false){
                this.validation.errorNotif( event, 'filename' );
                $( event.target ).prop('disabled', false);
                this.hideLoader( event );

                return;
            }

            this.editData({
                'id': $( '#imageID' ).val(),
                'title': title,
                'file': fileName,
                'oldFile': oldFileName,
                'fileName': file
            }, event );
        })
        .fail(() => {
            this.validation.errorNotif( event );
        });
    }

    editData( data, event )
    {
        this.imageAjax.update( data, 'update_entry' )
        .done((response) => {
            $( '#imageTitle' ).val( data.title );
            $( '#imageFilename' ).val( data.fileName );
            $( '#row-'+ data.id ).find('.imgRowTitle').text( data.title );
            $( '#row-'+ data.id ).find('img').attr( 'src', '/uploads/' + data.file );
            $( '#row-'+ data.id ).find('.imgRowFileName').text( data.file );
            this.oldForm = {
                'title': data.title,
                'file': data.fileName
            };

            $( event.target ).after('<div class="alert">Updated successfully!</div>');
            this.hideLoader( event );
        })
        .fail(() => {
            this.validation.errorNotif( event );
        });
    }

    removeNotif( eL )
    {
        $( eL ).find('.alert').remove();
    }

    showLoader( event )
    {
        $( event.target).parent().slideUp('fast', () => {
            $( event.target).parents(':eq(1)').find('.img-loader').fadeIn();
        });
    }

    hideLoader( event )
    {
        setTimeout(() => {
            $( event.target ).parents(':eq(1)').find('.img-loader').fadeOut().promise().done(() => {
                $( event.target ).parents(':eq(1)').find('form').slideDown('fast');
            });
        }, 500);
    }

    addRow( data )
    {
        let table = $('.image-upload-table table').length;

        if(table === undefined || table === 0){
            let content = '<table><thead><tr style="display:none;" class="animateShow">'
            + '<th><div style="display:none;">Title</div></th>'
            + '<th><div style="display:none;">Thumbnail</div></th>'
            + '<th><div style="display:none;">Filename</div></th>'
            + '<th><div style="display:none;">Date added</div></th>'
            + '<th><div style="display:none;"><input type="checkbox" id="selectAll"></div></th>'
            + '</tr></thead>'
            + '<tbody>'
            +'<tr id="row-'+ data.id + '" style="display:none;" class="animateShow">'
            + '<td><div style="display:none;"><span><a href="#modalEdit" class="editRow showModal" id="'+ data.id + '">Edit</a></span>'
            + '<span class="imgRowTitle">'+ data.title +'<span></div></td>'
            + '<td><div style="display:none;"><img src="/uploads/'+ data.file_name +'" alt="'+ data.title +'"></div></td>'
            + '<td><div style="display:none;"><span class="imgRowFileName">'+ data.file_name +'</span></div></td>'
            + '<td><div style="display:none;">'+ data.date +'</div></td>'
            + '<td><div style="display:none;"><input type="checkbox" value="'+ data.id +'" class="dataCheckbox"></div></td>'
            + '</tr>';

            $('.image-upload-table ').html(content);

            return;
        }

        let rowContent = '<tr id="row-'+ data.id + '" style="display:none;" class="animateShow">'
        + '<td><div style="display:none;"><span><a href="#modalEdit" class="editRow showModal" id="'+ data.id + '">Edit</a></span>'
        + '<span class="imgRowTitle">'+ data.title +'<span></div></td>'
        + '<td><div style="display:none;"><img src="/uploads/'+ data.file_name +'" alt="'+ data.title +'"></div></td>'
        + '<td><div style="display:none;"><span class="imgRowFileName">'+ data.file_name +'</span></div></td>'
        + '<td><div style="display:none;">'+ data.date +'</div></td>'
        + '<td><div style="display:none;"><input type="checkbox" value="'+ data.id +'" class="dataCheckbox"></div></td>'
        + '</tr>';

        $('.image-upload-table tbody tr:last-child').after(rowContent);
    }

    animateNewRows()
    {
        $( '.animateShow' ).each(function(i, val){
            setTimeout(function(){
                $(val).fadeIn();
                $(val).find('div').slideDown(500);
            }, 500 * i);
        });
    }
}

export default ImageUpload;