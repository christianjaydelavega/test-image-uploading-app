import $ from 'jquery';

class Validation {
    constructor()
    {
        this.validImgTypes = ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'webp', 'svg'];
    }

    required( el )
    {
        if( el.length <= 0 ){
            return false;
        }

        return true;
    }

    fileType( el )
    {
        if($.inArray(el, this.validImgTypes) == -1) {
            return false;
        }

        return true;
    }

    frmValid( oldData, newData )
    {
        if( JSON.stringify(oldData) === JSON.stringify(newData) ){
            return false;
        }

        return true;
    }

    errorNotif( event, type )
    {
        let message = this.errorMessage( type );
        
        $( event.target ).after('<div class="alert alert-error">' + message + '</div>');
    }

    errorMessage( type = 'default' )
    {
        let message = {
            'default': 'Something went wrong. Please try again later.',
            'requiredFile': 'Select a file.',
            'requiredFilename': 'Filename is required',
            'type': 'Invalid file type.',
            'filename': 'File name already exists.'
        };

        return message[type];
    }
    
}

export default Validation;