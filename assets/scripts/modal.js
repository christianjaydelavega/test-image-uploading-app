import $ from 'jquery';

class Modal {

    show( eL )
    {
        $( eL ).show().promise().done(() => {
            $( eL ).find('.modal').animate({bottom: "-20%"}, 300);
        });
    }

    close( eL )
    {
        $( eL ).animate({bottom: "0%"}, 300).promise().done(() => {
            $( eL ).parent().hide();
            this.reset( eL );
        });
    }

    reset( eL )
    {
        $( eL ).parent().find('input').val('');
        $( eL ).parent().find('.imageFileLabel').html('Choose an image');
        $( '#editBtn ').prop('disabled', true);
    }
}

export default Modal;