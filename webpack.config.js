const path = require("path");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const postcssPresetEnv = require("postcss-preset-env");

const devMode = process.env.NODE_ENV !== "production";

var pluginsConfig = [
  new MiniCssExtractPlugin({
    filename: devMode ? "main.css" : "main.css"
  })
];

var moduleConfig = {
  rules: [
    {
      test: /\.(sa|sc)ss$/,
      use: [
        {
          loader: MiniCssExtractPlugin.loader
        },
        {
          loader: "css-loader",
          options: {
            importLoaders: 2
          }
        },
        {
          loader: "postcss-loader",
          options: {
            ident: "postcss",
            plugins: devMode
              ? () => []
              : () => [
                  postcssPresetEnv({
                    browsers: [">1%"]
                  }),
                  require("cssnano")()
                ]
          }
        },
        {
          loader: "sass-loader"
        }
      ]
    },
    {
      test: /\.(png|jpe?g|gif)$/,
      use: [
        {
          loader: "file-loader",
          options: {
            name: "[name].[ext]",
            publicPath: "./assets/images",
            emitFile: false
          }
        }
      ]
    },
    {
      test: /\.(svg|woff|woff2|ttf|eot)(\?.*$|$)/,
      loader: "file-loader",
      options: {
        name: "fonts/[name].[ext]",
        publicPath: "./",
      }
    }
  ]
};

var main = {
  mode: devMode ? "development" : "production",
  entry: [
    "./assets/scripts/main.js",
    "./assets/scss/main.scss"
  ],
  output: {
    path: path.resolve(__dirname, "assets/dist/main"),
    publicPath: "/css",
    filename: "main.js"
  },
  module: moduleConfig,
  plugins: pluginsConfig
};

module.exports = [
  main
];