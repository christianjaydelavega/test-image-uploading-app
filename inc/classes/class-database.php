<?php

namespace TestIU;

class Database {
    
    private $user, $host, $pass, $db;

    public function __construct()
    {
        $this->user = DB_USER;
        $this->host = DB_HOST;
        $this->pass = DB_PASSWORD;
        $this->db = DB_NAME;
    }
    
    public function connect()
    {
        $connect = new \mysqli( $this->host, $this->user, $this->pass, $this->db );

        if($connect->connect_errno):
            echo "Failed to connect to MySQL: " . $connect->connect_error;
            exit();
        endif;

        return $connect;
    }

    public function get_uploads()
    {
        $mysqli = $this->connect();

        $query = "SELECT * FROM test_images";

        $response = $mysqli->query($query);
       
        $mysqli->close();
        
        if(!$response):
            return false;
        endif;
        
        return mysqli_fetch_all($response, MYSQLI_ASSOC);;
    }

    public function add( $data )
    {
        $mysqli = $this->connect();

        $query = $mysqli->prepare("INSERT INTO test_images ( title, file_name, date_added ) VALUES ( ?, ?, ?)");
        
        $query->bind_param( 'sss', $data['title'], $data['file_name'], $data['date'] );
        $query->execute();

        $id = $mysqli->insert_id;

        $mysqli->close();
        
        return $id;
    }

    public function delete( $id )
    {
        $mysqli = $this->connect();
        
        $response = $mysqli->query("DELETE FROM test_images WHERE ID IN ( $id )");

        $mysqli->close();
        
        return $response;
    }

    public function update( $data )
    {
        $mysqli = $this->connect();

        $query = $mysqli->prepare("UPDATE test_images SET title = ?, file_name = ? WHERE ID = ? ");
        
        $query->bind_param( 'sss', $data['title'], $data['file_name'], $data['id'] );
        $query->execute();

        $mysqli->close();
        
        return $data['id'];
    }
}