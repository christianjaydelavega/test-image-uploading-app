<?php

namespace TestIU;
use TestIU\Database as Database;

class AJAX_Actions {

    private $db;

    public function init()
    {
        if(!$_POST || !isset($_POST['action']) || !$_POST['action']):
            return false;
        endif;

        if(!method_exists($this, $_POST['action'])):
            return false;
        endif;
        
        include '../../inc/_autoload.php';
        include '../../inc/config.php';

        $this->db = new Database();

        $this->{$_POST['action']}();

        exit();
    }

    public function upload_image()
    {
        $response = [
            'status' => false
        ];

        if(!$_FILES):
            header("HTTP/1.1 404 Not Found");
            echo json_encode( $response );
        endif;

        $allowed_types = [
            'image/jpeg',
            'image/jpg',
            'image/png',
            'image/gif',
            'image/bmp',
            'image/webp',
            'image/svg+xml'
        ];

        if(!in_array($_FILES['image']['type'], $allowed_types)):
            header("HTTP/1.1 406 Not Acceptable");
            echo json_encode( $response );

            return;
        endif;  
        
        $path = TEST_IU_DIR . '/uploads/';
        $name = $_FILES['image']['name'];

        if(file_exists($path . $name)):
            $name = $this->generate_image_name( $name, $path);
        endif;

        if(!move_uploaded_file( $_FILES['image']['tmp_name'], $path . $name )):
            header("HTTP/1.1 406 Not Acceptable");
            echo json_encode( $response );

            return;
        endif;
        
        $title = !empty($_POST['image_title']) ? $_POST['image_title'] : 
            substr($_FILES['image']['name'], 0 , (strrpos($_FILES['image']['name'], ".")));
        
        $data = [
            'title' => $title,
            'file_name' => $name,
            'date' => date("Y-m-d h:i:s")
        ];

        $add = $this->db->add( $data );

        if(!$add):
            header("HTTP/1.1 406 Not Acceptable");
            echo json_encode( $response );

            return;
        endif;

        $data['id'] = $add;
        
        $response = [
            'status' => true,
            'data' => $data
        ];

        echo json_encode( $response );
    }

    public function delete_image()
    {
        $response = [
            'status' => false
        ];

        if(!$_POST['ids'] || !$_POST['images']):
            header("HTTP/1.1 404 Not Found");
            echo json_encode( $response );
        endif;

        $delete = $this->db->delete( $_POST['ids'] );

        if(!$delete):
            header("HTTP/1.1 406 Not Acceptable");
            echo json_encode( $response );

            return;
        endif;

        $images = explode(',', $_POST['images']);
        
        foreach( $images as $img ):
            if(!file_exists(TEST_IU_DIR . $img)):
                continue;
            endif;
                
            unlink( TEST_IU_DIR . $img );
        endforeach;
        
        $response = [
            'status' => true,
            'data' => $_POST['ids']
        ];

        echo json_encode( $response );
    }

    public function is_name_valid()
    {
        $response = [
            'status' => false
        ];

        if(!$_POST['name']):
            header("HTTP/1.1 404 Not Found");
            echo json_encode( $response );
        endif;
        
        $name = TEST_IU_DIR .'/uploads/' . trim($_POST['name']);

        if(file_exists( $name )):
            echo json_encode( $response );
            return;
        endif;

        $response = [
            'status' => true
        ];

        echo json_encode( $response );
    }

    public function update_entry()
    {
        $response = [
            'status' => false
        ];

        if(!$_POST['id'] || !$_POST['file'] || !$_POST['old_file']):
            header("HTTP/1.1 404 Not Found");
            echo json_encode( $response );
        endif;

        $edit = $this->db->update([
            'id' => $_POST['id'],
            'title' => trim($_POST['title']),
            'file_name' => trim($_POST['file']),
        ]);

        if(!$edit):
            header("HTTP/1.1 406 Not Acceptable");
            echo json_encode( $response );

            return;
        endif;

        $rename = rename( TEST_IU_DIR .'/uploads/' . trim($_POST['old_file']), TEST_IU_DIR .'/uploads/' . trim($_POST['file']) );

        if(!$edit):
            header("HTTP/1.1 406 Not Acceptable");
            echo json_encode( $response );

            return;
        endif;
        
        $response = [
            'status' => true,
            'data' => $_POST['id']
        ];

        echo json_encode( $response );
    }

    public function generate_image_name( $temp_name, $path )
    {
        $ext = pathinfo($temp_name, PATHINFO_EXTENSION);
        $name = str_replace('.' . $ext, '-' . rand(1, 10000) . '.' . $ext, $temp_name);

        if(file_exists($path . $name)):
            $this->generate_image_name( $temp_name, $path );
        endif;

        return $name;
    }
}

$ajax_actions = new AJAX_Actions();
$ajax_actions->init();