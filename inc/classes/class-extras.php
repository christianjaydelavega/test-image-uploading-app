<?php

namespace TestIU;

class Extras {

    public static function assets_on_header()
    {
        $stylesheets = [
            'main-css' => '/assets/dist/main/main.css'
        ];
        
        foreach( $stylesheets as $key => $style ):
            echo sprintf( "<link rel='stylesheet' id='%s' href='%s' media='all'>",
                $key,
                $style);
        endforeach;
    }

    public static function assets_on_footer()
    {
        $scripts = [
            'main-js' => '/assets/dist/main/main.js'
        ];
        
        foreach( $scripts as $key => $script ):
            echo sprintf( "<script src='%s' id='%s'></script>",
                $script,
                $key);
        endforeach;
    }

    public static function get_view( $file, $val = '' )
    {   
        $value = $val;
        
        include( TEST_IU_DIR . "/views/$file.php");
    }
}