<?php

define( 'TEST_IU_DIR', realpath(dirname(__FILE__) . '/..'));

//Autoload Files
spl_autoload_register( function( $class) {
    $prefix = 'TestIU';
    $len = strlen($prefix);
    
    if(strncmp($prefix,$class, $len) !== 0 ) :
        return;
    endif;

    $path_arr = explode('\\', $class);
    $class_name = str_replace('_', '-', end($path_arr));
    array_pop($path_arr);
    $path = '';
    
    foreach($path_arr as $rclass):
        $path .= '/' . str_replace('_', '-', $rclass);
    endforeach;
    
    $allowed_file_prefix = [
        'class-'
    ];

    foreach( $allowed_file_prefix as $prefix ) :
        $check_location = TEST_IU_DIR . '/inc/classes' . str_replace('/testiu','', strtolower($path))  . "/$prefix" . strtolower($class_name) . '.php';

        if(file_exists( $check_location )):
            require_once $check_location;
            return;
        endif;
    endforeach;
});