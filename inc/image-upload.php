<?php

namespace TestIU;
use TestIU\Extras;
use TestIU\Database as Database;

class Image_Upload {

    public $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    public function init()
    {   
        //load site header
        Extras::get_view( 'header' );

        //load image upload top buttons
        Extras::get_view( 'image-upload-buttons' );

        //load image upload table
        $this->load_uploads();
        
        //load site footer
        Extras::get_view( 'footer' );
    }

    public function load_uploads()
    {
        $uploads = $this->db->get_uploads();
        
        if(!$uploads):
            echo '<div class="image-upload-table"><h2>No uploads yet.</h2> <span class="highlighted">Upload one now <i class="fa fa-hand-point-up"></i><span></div>';
            return;
        endif;

        ob_start();

        foreach( $uploads as $key => $upload ):
            Extras::get_view( 'image-upload-table-row', [
                'id' => $upload['ID'],
                'title' => $upload['title'],
                'img' => '/uploads/' . $upload['file_name'],
                'file_name' => $upload['file_name'],
                'date' => $upload['date_added']
            ]);
        endforeach;

        Extras::get_view( 'image-upload-table', ob_get_clean() );
    }
}