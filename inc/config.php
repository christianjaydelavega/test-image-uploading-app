<?php

if (!defined('DB_HOST')):
    define('DB_HOST', 'localhost');
endif;

if (!defined('DB_USER')):
    define('DB_USER', 'root');
endif;

if (!defined('DB_PASSWORD')):
    define('DB_PASSWORD', 'root');
endif;

if (!defined('DB_NAME')):
    define('DB_NAME', 'test_img_upload');
endif;